<?xml version="1.0"?>
<!DOCTYPE book PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN" "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd">
<book>

<chapter id="BASIC">
<chapterinfo><author><firstname>bulia</firstname><surname>byak</surname><email>buliabyak@users.sf.net</email></author>
<!-- Copyright (C) 2004-5 bulia byak, buliabyak@users.sf.net -->
</chapterinfo>
<title>Basic</title>

<abstract>
<para>This tutorial demonstrates the basics of using Inkscape. This is a 
regular Inkscape document that you can view, edit, copy from, or save.
</para>

<para>The Basic Tutorial covers canvas navigation, managing documents, shape tool
basics, selection techniques, transforming objects with selector, grouping, setting fill
and stroke, alignment, and z-order. For more advanced topics, check out the other
tutorials in the <guimenu>Help</guimenu> menu.
</para>
</abstract>


<sect1>
<title>Panning the canvas</title>

<para>There are many ways to pan (scroll) the document canvas. Try
<keycombo><keycap function="control">Ctrl</keycap><keycap>arrow</keycap></keycombo> keys to scroll by keyboard. (Try this now to scroll this
document down.) You can also drag the canvas by the middle mouse
button. Or, you can use the scrollbars (press <keycombo><keycap function="control">Ctrl</keycap><keycap>B</keycap></keycombo> to show or hide
them). The <mousebutton role="mouse-wheel">wheel</mousebutton> on your mouse also works for scrolling vertically;
press <keycap function="shift">Shift</keycap> with the wheel to scroll horizontally.</para>
</sect1>

<sect1>
<title>Zooming in or out</title>
<para>The easiest way to zoom is by pressing <keycap>-</keycap> and <keycap>+</keycap> (or
<keycap>=</keycap>) keys.  You can also use <keycombo><keycap function="control">Ctrl</keycap><mousebutton role="middle-click">middle click</mousebutton></keycombo> or
<keycombo><keycap function="control">Ctrl</keycap><mousebutton role="right-click">right click</mousebutton></keycombo> to zoom in, <keycombo><keycap function="shift">Shift</keycap><mousebutton role="middle-click">middle click</mousebutton></keycombo> or
<keycombo><keycap function="shift">Shift</keycap><mousebutton role="right-click">right click</mousebutton></keycombo> to zoom out, or rotate the mouse wheel with
<keycap function="control">Ctrl</keycap>. Or, you can click in the zoom entry field (in the bottom right
corner of the document window), type a precise zoom value in %, and press <keycap>Enter</keycap>. We also
have the Zoom tool (in the toolbar on left) which lets you to zoom into an area by
dragging around it.
</para>
<para>Inkscape also keeps a history of the zoom levels you've used in this work
session. Press the <keycap>`</keycap> key to go back to the previous zoom, or
<keycombo><keycap function="shift">Shift</keycap><keycap>`</keycap></keycombo> to go forward.
</para>
</sect1>

<sect1>
<title>Inkscape tools</title> 

<para>The vertical toolbar on the left shows Inkscape's drawing and editing tools. In
the top part of the window, below the menu, there's the <firstterm>Commands
bar</firstterm> with general command buttons and the <firstterm>Tool Controls
bar</firstterm> with controls that are specific to each tool. The <firstterm>status
bar</firstterm> at the bottom of the window will display useful hints and messages as
you work.</para>

<para>Many operations are available through keyboard shortcuts. Open
<menuchoice><guimenu>Help</guimenu><guimenuitem>Keys and Mouse Reference</guimenuitem></menuchoice> to see the complete reference.</para>
</sect1>

<sect1>
<title>Creating and managing documents</title>

<para>To create a new empty document, use <menuchoice><guimenu>File</guimenu><guimenuitem>New</guimenuitem></menuchoice> 
or press <keycombo><keycap function="control">Ctrl</keycap><keycap>N</keycap></keycombo>. To create a new document from one of Inkscape's many 
templates, use <menuchoice><guimenu>File</guimenu><guimenuitem>New from Template…</guimenuitem></menuchoice> or press 
<keycombo><keycap function="control">Ctrl</keycap><keycap function="alt">Alt</keycap><keycap>N</keycap></keycombo></para>

<para>To open an existing SVG document, use <menuchoice><guimenu>File</guimenu><guimenuitem>Open</guimenuitem></menuchoice> (<keycombo><keycap function="control">Ctrl</keycap><keycap>O</keycap></keycombo>). To save, use <menuchoice><guimenu>File</guimenu><guimenuitem>Save</guimenuitem></menuchoice>
(<keycombo><keycap function="control">Ctrl</keycap><keycap>S</keycap></keycombo>), or <menuchoice><guimenu>File</guimenu><guimenuitem>Save As</guimenuitem></menuchoice> (<keycombo><keycap function="shift">Shift</keycap><keycap function="control">Ctrl</keycap><keycap>S</keycap></keycombo>)
to save under a new name.  (Inkscape may still be unstable, so remember to save
often!)</para>

<para>Inkscape uses the SVG (Scalable Vector Graphics) format for its files. SVG is an open standard widely 
supported by graphic software. SVG files are based on XML and can be edited with any
text or XML editor (apart from Inkscape, that is). Besides SVG,
Inkscape can import and export several other formats (EPS, PNG).</para> 

<para>Inkscape opens a separate document window for each document. You can navigate
among them using your window manager (e.g. by <keycombo><keycap function="alt">Alt</keycap><keycap>Tab</keycap></keycombo>), or you can use
the Inkscape shortcut, <keycombo><keycap function="control">Ctrl</keycap><keycap>Tab</keycap></keycombo>, which will cycle through all open
document windows. (Create a new document now and switch between it and this document for
practice.) Note: Inkscape treats these windows like tabs in a web browser, this means
the <keycombo><keycap function="control">Ctrl</keycap><keycap>Tab</keycap></keycombo> shortcut only works with documents running in the same
process. If you open multiple files from a file browser or launch more than
one Inkscape process from an icon it will not work.</para>
</sect1>

<sect1>
<title>Creating shapes</title>
<para>Time for some nice shapes! Click on the Rectangle tool in the toolbar
(or press <keycap>F4</keycap>) and click-and-drag, either in a new empty document or
right here:</para>

<informalfigure>
<mediaobject>
<imageobject><imagedata fileref="basic-f01.svg"/>
</imageobject>
</mediaobject>
</informalfigure>

<para>As you can see, default rectangles come up blue, with a black <firstterm>stroke</firstterm> (outline),
and fully opaque. We'll see how to change that below. With other tools, you can
also create ellipses, stars, and spirals:</para>

<informalfigure>
<mediaobject>
<imageobject><imagedata fileref="basic-f02.svg"/>
</imageobject>
</mediaobject>
</informalfigure>

<para>These tools are collectively known as <firstterm>shape tools</firstterm>. Each shape you
create displays one or more diamond-shaped <firstterm>handles</firstterm>; try dragging
them to see how the shape responds. The Controls panel for a shape tool is another way to tweak a shape;
these controls affect the currently selected shapes (i.e. those that display the handles) 
<emphasis>and</emphasis> set the default that will apply to newly created shapes.</para>

<para>To <firstterm>undo</firstterm> your last action, press
<keycombo><keycap function="control">Ctrl</keycap><keycap>Z</keycap></keycombo>. (Or, if you change your mind again, you can
<firstterm>redo</firstterm> the undone action by <keycombo><keycap function="shift">Shift</keycap><keycap function="control">Ctrl</keycap><keycap>Z</keycap></keycombo>.)</para>
</sect1>


<sect1>
<title>Moving, scaling, rotating</title> <para>The most frequently used Inkscape tool is
the <firstterm>Selector</firstterm>. Click the topmost button (with the arrow) on the
toolbar, or press <keycap>F1</keycap> or <keycap>Space</keycap>.  Now you can select
any object on the canvas. Click on the rectangle below.</para>

<informalfigure>
<mediaobject>
<imageobject><imagedata fileref="basic-f03.svg"/>
</imageobject>
</mediaobject>
</informalfigure>

<para>You will see eight arrow-shaped handles appear around the object.
Now you can:</para>
<itemizedlist>
<listitem>
<para><firstterm>Move</firstterm> the object by dragging it. (Press <keycap function="control">Ctrl</keycap> to restrict movement
to horizontal and vertical.)</para>
</listitem>
<listitem>
<para><firstterm>Scale</firstterm> the object by dragging any handle. (Press <keycap function="control">Ctrl</keycap> to preserve
the original height/width ratio.)
</para></listitem>
</itemizedlist>

<para>Now click the rectangle again. The handles change. Now you can:</para>

<itemizedlist>
<listitem>
<para><firstterm>Rotate</firstterm> the object by dragging corner handles. (Press <keycap function="control">Ctrl</keycap> to
restrict rotation to 15 degree steps. Drag the cross mark to 
position the center of rotation.)</para>
</listitem>
<listitem>
<para><firstterm>Skew</firstterm> (shear) the object by dragging non-corner
handles. (Press <keycap function="control">Ctrl</keycap> to restrict skewing to 15 degree
steps.)</para></listitem>
</itemizedlist>

<para>While in Selector, you can also use the numeric entry fields in the Controls bar
(above the canvas) to set exact values for coordinates (X and Y) and size (W and H) of
the selection.</para>
</sect1>

<sect1>
<title>Transforming by keys</title> 

<para>One of Inkscape's features that set it apart from most other vector editors is its
emphasis on keyboard accessibility. There's hardly any command or action that is
impossible to do from keyboard, and transforming objects is no exception.</para>

<para>You can use the keyboard to move (<keycap>arrow</keycap> keys), scale
(<keycap>&lt;</keycap> and <keycap>&gt;</keycap> keys), and rotate (<keycap>[</keycap>
and <keycap>]</keycap> keys) objects. Default moves and scales are by 2 px; with
<keycap function="shift">Shift</keycap>, you move by 10 times that. <keycombo><keycap function="control">Ctrl</keycap><keycap>&gt;</keycap></keycombo>
and <keycombo><keycap function="control">Ctrl</keycap><keycap>&lt;</keycap></keycombo> scale up or down to 200% or 50% of the original,
respectively.  Default rotates are by 15 degrees; with <keycap function="control">Ctrl</keycap>, you rotate
by 90 degrees.</para>

<para>However, perhaps the most useful are <firstterm>pixel-size
transformations</firstterm>, invoked by using <keycap function="alt">Alt</keycap> with the transform
keys. For example, <keycombo><keycap function="alt">Alt</keycap><keycap>arrows</keycap></keycombo> will move the selection by 1 pixel
<emphasis>at the current zoom</emphasis> (i.e. by 1 <firstterm>screen pixel</firstterm>,
not to be confused with the px unit which is an SVG length unit independent of
zoom). This means that if you zoom in, one <keycombo><keycap function="alt">Alt</keycap><keycap>arrow</keycap></keycombo> will result in a
<emphasis>smaller</emphasis> absolute movement which will still look like one-pixel
nudge on your screen. It is thus possible to position objects with arbitrary precision
simply by zooming in or out as needed.</para>

<para>Similarly, <keycombo><keycap function="alt">Alt</keycap><keycap>&gt;</keycap></keycombo> and <keycombo><keycap function="alt">Alt</keycap><keycap>&lt;</keycap></keycombo>
scale selection so that its visible size changes by one screen pixel, and
<keycombo><keycap function="alt">Alt</keycap><keycap>[</keycap></keycombo> and <keycombo><keycap function="alt">Alt</keycap><keycap>]</keycap></keycombo> rotate it so that its
farthest-from-center point moves by one screen pixel.</para>

<para>Note: Linux users may not get the expected results with the <keycombo><keycap function="alt">Alt</keycap><keycap>arrow</keycap></keycombo> and a few other key combinations if their Window Manager catches those key events before they reach the inkscape application. One solution would be to change the WM's configuration accordingly.</para>
</sect1>

<sect1>
<title>Multiple selections</title> <para>You can select any number of objects
simultaneously by <keycombo><keycap function="shift">Shift</keycap><mousebutton role="click">click</mousebutton></keycombo>ing them. Or, you can
<mousebutton role="mouse-drag">drag</mousebutton> around the objects you need to select; this is called
<firstterm>rubberband selection</firstterm>. (Selector creates rubberband when dragging
from an empty space; however, if you press <keycap function="shift">Shift</keycap> before starting to
drag, Inkscape will always create the rubberband.) Practice by selecting all three of
the shapes below:</para>

<informalfigure>
<mediaobject>
<imageobject><imagedata fileref="basic-f04.svg"/>
</imageobject>
</mediaobject>
</informalfigure>

<para>Now, use rubberband (by drag or <keycombo><keycap function="shift">Shift</keycap><mousebutton role="mouse-drag">drag</mousebutton></keycombo>) to select the two ellipses
but not the rectangle:</para>

<informalfigure>
<mediaobject>
<imageobject><imagedata fileref="basic-f05.svg"/>
</imageobject>
</mediaobject>
</informalfigure>

<para>Each individual object within a selection displays a <firstterm>selection
cue</firstterm> &#8212; by default, a dashed rectangular frame. These cues make it easy
to see at once what is selected and what is not. For example, if you select both the two
ellipses and the rectangle, without the cues you would have hard time guessing whether
the ellipses are selected or not.</para>

<para><keycombo><keycap function="shift">Shift</keycap><mousebutton role="click">click</mousebutton></keycombo>ing on a selected object excludes it from the selection. 
Select all three objects above, then use <keycombo><keycap function="shift">Shift</keycap><mousebutton role="click">click</mousebutton></keycombo> to exclude both
ellipses from the selection leaving only the rectangle selected.</para>

<para>Pressing <keycap>Esc</keycap> deselects any selected
objects. <keycombo><keycap function="control">Ctrl</keycap><keycap>A</keycap></keycombo> selects all objects in the current layer (if you did
not create layers, this is the same as all objects in the document).</para>
</sect1>

<sect1>
<title>Grouping</title>

<para>Several objects can be combined into a <firstterm>group</firstterm>. A group
behaves as a single object when you drag or transform it. Below, the three objects on
the left are independent; the same three objects on the right are grouped. Try to drag
the group.</para>

<informalfigure>
<mediaobject>
<imageobject><imagedata fileref="basic-f06.svg"/>
</imageobject>
</mediaobject>
</informalfigure>

<para>To create a group, you select one or more objects and press
<keycombo><keycap function="control">Ctrl</keycap><keycap>G</keycap></keycombo>. To ungroup one or more groups, select them and press
<keycombo><keycap function="control">Ctrl</keycap><keycap>U</keycap></keycombo>.  Groups themselves may be grouped, just like any other
objects; such recursive groups may go down to arbitrary depth. However,
<keycombo><keycap function="control">Ctrl</keycap><keycap>U</keycap></keycombo> only ungroups the topmost level of grouping in a
selection; you'll need to press <keycombo><keycap function="control">Ctrl</keycap><keycap>U</keycap></keycombo> repeatedly if you want to
completely ungroup a deep group-in-group.</para>

<para>You don't necessarily have to ungroup, however, if you want to edit
an object within a group. Just <keycombo><keycap function="control">Ctrl</keycap><mousebutton role="click">click</mousebutton></keycombo> that object and it will be
selected and editable alone, or <keycombo><keycap function="shift">Shift</keycap><keycap function="control">Ctrl</keycap><mousebutton role="click">click</mousebutton></keycombo> several objects
(inside or outside any groups) for multiple selection regardless of
grouping. Try to move or transform the individual shapes in the
group (above right) without ungrouping it, then deselect and select
the group normally to see that it still remains grouped.</para>
</sect1>

<sect1>
<title>Fill and stroke</title> 

<para>Probably the simplest way to paint an object some color is to
select an object, and click a swatch in the palette below the canvas to
paint it (change its fill color).

Alternatively, you can open the Swatches dialog from the
<guimenu>View</guimenu> menu (or press <keycombo><keycap function="shift">Shift</keycap><keycap function="control">Ctrl</keycap><keycap>W</keycap></keycombo>), select an object, and click a swatch to paint
it (change its fill color).</para>

<para>More powerful is the Fill and Stroke dialog from the <guimenu>Object</guimenu> menu
(or press <keycombo><keycap function="shift">Shift</keycap><keycap function="control">Ctrl</keycap><keycap>F</keycap></keycombo>). Select the shape below and open the Fill and Stroke
dialog.</para>

<informalfigure>
<mediaobject>
<imageobject><imagedata fileref="basic-f07.svg"/>
</imageobject>
</mediaobject>
</informalfigure>

<para>You will see that the dialog has three tabs: Fill, Stroke paint, and Stroke
style. The Fill tab lets you edit the <firstterm>fill</firstterm> (interior) of the
selected object(s). Using the buttons just below the tab, you can select types of fill,
including no fill (the button with the X), flat color fill, as well as linear or radial
gradients. For the above shape, the flat fill button will be activated.</para>

<para>Further below, you see a collection of <firstterm>color pickers</firstterm>, each
in its own tab: RGB, CMYK, HSL, and Wheel. Perhaps the most convenient is the Wheel
picker, where you can rotate the triangle to choose a hue on the wheel, and then select
a shade of that hue within the triangle. All color pickers contain a slider to set the
<firstterm>alpha</firstterm> (opacity) of the selected object(s).</para>

<para>Whenever you select an object, the color picker is updated to display its current
fill and stroke (for multiple selected objects, the dialog shows their
<emphasis>average</emphasis> color). Play with these samples or create your own:</para>

<informalfigure>
<mediaobject>
<imageobject><imagedata fileref="basic-f08.svg"/>
</imageobject>
</mediaobject>
</informalfigure>

<para>Using the Stroke paint tab, you can remove the <firstterm>stroke</firstterm>
(outline) of the object, or assign any color or transparency to it:</para>

<informalfigure>
<mediaobject>
<imageobject><imagedata fileref="basic-f09.svg"/>
</imageobject>
</mediaobject>
</informalfigure>

<para>The last tab, Stroke style, lets you set the width and other parameters
of the stroke:</para>

<informalfigure>
<mediaobject>
<imageobject><imagedata fileref="basic-f10.svg"/>
</imageobject>
</mediaobject>
</informalfigure>

<para>Finally, instead of flat color, you can use <firstterm>gradients</firstterm> for
fills and/or strokes:</para>

<informalfigure>
<mediaobject>
<imageobject><imagedata fileref="basic-f11.svg"/>
</imageobject>
</mediaobject>
</informalfigure>

<para>When you switch from flat color to gradient, the newly created gradient uses the
previous flat color, going from opaque to transparent. Switch to the Gradient tool
(<keycombo><keycap function="control">Ctrl</keycap><keycap>F1</keycap></keycombo>) to drag the <firstterm>gradient handles</firstterm> &#8212;
the controls connected by lines that define the direction and length of the
gradient. When any of the gradient handles is selected (highlighted blue), the Fill and
Stroke dialog sets the color of that handle instead of the color of the entire selected
object.</para>

<para>Yet another convenient way to change a color of an object is by using the Dropper
tool (<keycap>F7</keycap>). Just <mousebutton role="click">click</mousebutton> anywhere in the drawing with
that tool, and the picked color will be assigned to the selected object's fill
(<keycombo><keycap function="shift">Shift</keycap><mousebutton role="click">click</mousebutton></keycombo> will assign stroke color).</para>
</sect1>

<sect1>
<title>Duplication, alignment, distribution</title>

<para>One of the most common operations is <firstterm>duplicating</firstterm> an object
(<keycombo><keycap function="control">Ctrl</keycap><keycap>D</keycap></keycombo>). The duplicate is placed exactly above the original and is
selected, so you can drag it away by <mousebutton>mouse</mousebutton> or by <keycap>arrow</keycap> keys. For practice, try to fill
the line with copies of this black square:</para>

<informalfigure>
<mediaobject>
<imageobject><imagedata fileref="basic-f12.svg"/>
</imageobject>
</mediaobject>
</informalfigure>

<para>Chances are, your copies of the square are placed more or less randomly. This is
where the Align and Distribute dialog (<keycombo><keycap function="shift">Shift</keycap><keycap function="control">Ctrl</keycap><keycap>A</keycap></keycombo>) is useful. Select all the squares
(<keycombo><keycap function="shift">Shift</keycap><mousebutton role="click">click</mousebutton></keycombo> or drag a rubberband), open the dialog and press the
“Center on horizontal axis” button, then the “Make horizontal gaps between objects
equal” button (read the button tooltips). The objects are now neatly aligned and
distributed equispacedly. Here are some other alignment and distribution
examples:</para>

<informalfigure>
<mediaobject>
<imageobject><imagedata fileref="basic-f13.svg"/>
</imageobject>
</mediaobject>
</informalfigure>

</sect1>

<sect1>
<title>Z-order</title>

<para>The term <firstterm>z-order</firstterm> refers to the stacking order of objects in a drawing,
i.e. to which objects are on top and obscure others. The two commands in the <guimenu>Object</guimenu>
menu, <guimenuitem>Raise to Top</guimenuitem> (the <keycap>Home</keycap> key) and Lower to Bottom (the
<keycap>End</keycap> key), will move your selected objects to the very top or very
bottom of the current layer's z-order. Two more commands, <guimenuitem>Raise</guimenuitem> (<keycap>PgUp</keycap>)
and <guimenuitem>Lower</guimenuitem> (<keycap>PgDn</keycap>), will sink or emerge the selection <emphasis>one step only</emphasis>,
i.e. move it past one non-selected object in z-order (only objects that overlap the
selection count, based on their respective bounding boxes).</para>

<para>Practice using these commands by reversing the z-order of the
objects below, so that the leftmost ellipse is on top and the rightmost
one is at the bottom:</para>

<informalfigure>
<mediaobject>
<imageobject><imagedata fileref="basic-f14.svg"/>
</imageobject>
</mediaobject>
</informalfigure>

<para>A very useful selection shortcut is the <keycap>Tab</keycap> key. If nothing is
selected, it selects the bottommost object; otherwise it selects the object <emphasis>above the
selected object(s)</emphasis> in z-order.  <keycombo><keycap function="shift">Shift</keycap><keycap>Tab</keycap></keycombo> works in reverse,
starting from the topmost object and proceeding downwards. Since the objects you
create are added to the top of the stack, pressing <keycombo><keycap function="shift">Shift</keycap><keycap>Tab</keycap></keycombo> with
nothing selected will conveniently select the object you created <emphasis>last</emphasis>. Practice
the <keycap>Tab</keycap> and <keycombo><keycap function="shift">Shift</keycap><keycap>Tab</keycap></keycombo> keys on the stack of ellipses
above.</para>
</sect1>

<sect1>
<title>Selecting under and dragging selected</title>

<para>What to do if the object you need is hidden behind another object?
You may still see the bottom object if the top one is (partially)
transparent, but clicking on it will select the top object, not the
one you need.</para>

<para>This is what <keycombo><keycap function="alt">Alt</keycap><mousebutton role="click">click</mousebutton></keycombo> is for. First <keycombo><keycap function="alt">Alt</keycap><mousebutton role="click">click</mousebutton></keycombo>
selects the top object just like the regular click. However, the next
<keycombo><keycap function="alt">Alt</keycap><mousebutton role="click">click</mousebutton></keycombo> at the same point will select the object <emphasis>below</emphasis> the top
one; the next one, the object still lower, etc. Thus, several
<keycombo><keycap function="alt">Alt</keycap><mousebutton role="click">click</mousebutton></keycombo>s in a row will cycle, top-to-bottom, through the entire
z-order stack of objects at the click point. When the bottom object is reached, next
<keycombo><keycap function="alt">Alt</keycap><mousebutton role="click">click</mousebutton></keycombo> will, naturally, again select the topmost object.</para>

<para>[If you are on Linux, you might find that
<keycombo><keycap function="alt">Alt</keycap><mousebutton role="click">click</mousebutton></keycombo> does not work properly.  Instead, it might be
moving the whole Inkscape window.  This is because your window manager
has reserved <keycombo><keycap function="alt">Alt</keycap><mousebutton role="click">click</mousebutton></keycombo> for a different action.  The way to fix this is
to find the Window Behavior configuration for your window manager, and
either turn it off, or map it to use the <keycap>Meta</keycap> key (aka <keycap>Windows</keycap> key), so
Inkscape and other applications may use the <keycap function="alt">Alt</keycap> key freely.]
</para>

<para>This is nice, but once you selected an under-the-surface object, what can you do
with it? You can use keys to transform it, and you can drag the selection
handles. However, dragging the object itself will reset the selection to the top object
again (this is how click-and-drag is designed to work &#8212; it selects the (top)
object under cursor first, then drags the selection). To tell Inkscape to drag <emphasis>what
is selected now</emphasis> without selecting anything else, use <keycombo><keycap function="alt">Alt</keycap><mousebutton role="mouse-drag">drag</mousebutton></keycombo>.
This will move the current selection no matter where you drag your mouse.</para>

<para>Practice <keycombo><keycap function="alt">Alt</keycap><mousebutton role="click">click</mousebutton></keycombo> and <keycombo><keycap function="alt">Alt</keycap><mousebutton role="mouse-drag">drag</mousebutton></keycombo> on the two brown
shapes under the green transparent rectangle:</para>

<informalfigure>
<mediaobject>
<imageobject><imagedata fileref="basic-f15.svg"/>
</imageobject>
</mediaobject>
</informalfigure>

</sect1>

<sect1>
<title>Selecting similar objects</title>
<para>Inkscape can select other objects similar to the object currently selected. 
For example, if you want to select all the blue squares below first select one of the 
blue squares, and use <menuchoice><guimenu>Edit</guimenu><guisubmenu>Select Same</guisubmenu><guimenuitem>Fill Color</guimenuitem></menuchoice> from the menu. All the objects with a fill color the same
shade of blue are now selected.</para>

<informalfigure>
<mediaobject>
<imageobject><imagedata fileref="basic-f16.svg"/>
</imageobject>
</mediaobject>
</informalfigure>

<para>In addition to selecting by fill color, you can select multiple similar objects
by stroke color, stroke style, fill &amp; stroke, and object type.</para>

</sect1>

<sect1>
<title>Conclusion</title>

<para>This concludes the Basic tutorial. There's much more than that to Inkscape, but
with the techniques described here, you will already be able to create simple yet useful
graphics. For more complicated stuff, go through the Advanced and other tutorials in
<menuchoice><guimenu>Help</guimenu><guimenuitem>Tutorials</guimenuitem></menuchoice>.
</para>
</sect1>



</chapter>
</book>
