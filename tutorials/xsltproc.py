#!/usr/bin/env python
# -*- coding: utf-8 -*-

# xsltproc.py - a Python version of xsltproc
#   as it's using lxml (and thereby libxml2/libxslt) output should be
#   identical to the output produced by xsltproc with one exeption:
#   We can implement our own XPath and XSLT extensions in Python!
#
#
#   Copyright (C) 2017  Eduard Braun <eduard.braun2@gmx.de>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#
# Documentation:
#   - http://lxml.de/xpathxslt.html - using XPath/XSLT with lxml
#   - http://lxml.de/extensions.html - implementing XPath/XSLT extensions


from __future__ import print_function   # use print() as a function in Python 2 (see PEP 3105)
from __future__ import absolute_import  # use absolute imports by default in Python 2 (see PEP 328)

import argparse
import os.path
import shlex
import subprocess
import sys

import lxml.etree as etree
import pexpect.popen_spawn


# class for XPath extension functions
#   - we require those for creating tutorials
#   - python function names are directly mapped to the XPath function name
class XPathExtensions(object):
    # check if a file exists and return the result
    def exists(self, context, arg):
        return os.path.isfile(arg)

    # run a command and return the output from stdout
    def run(self, context, arg_cmd, arg_args):
        args = shlex.split(arg_args)
        try:
            return subprocess.check_output([arg_cmd] + args)
        except:
            print("Error: failed to execute command line '%s %s'" % (arg_cmd, arg_args), file=sys.stderr)
            raise

    # run Inkscape in interactive shell mode and return the output from stdout
    #   the same instance will be re-used for all calls in this session
    #   making it significantly faster than individual invocations using run()
    def run_shell(self, context, arg_cmd, arg_args):
        if not hasattr(self, 'p'):
            try:
                self.p = pexpect.popen_spawn.PopenSpawn(arg_cmd + ' --shell', timeout=60)
                self.p.expect('>')
            except:
                print("Error: failed to start '%s'" % arg_cmd, file=sys.stderr)
                raise
        self.p.sendline(arg_args)
        try:
            self.p.expect('>')
        except pexpect.ExceptionPexpect as err:
            print("Error: failed to execute shell command '%s'.\n\nReason: %s\n\nLast output from '%s': %s" %
                  (arg_args, err, arg_cmd, self.p.before), file=sys.stderr)
            raise
        return self.p.before

    # return value of variable with name 'arg_name'
    def get_variable(self, context, arg_name):
        return context.eval_context[arg_name]

    # set and return value of variable with name 'arg_name' to value 'arg_value'
    def set_variable(self, context, arg_name, arg_value):
        context.eval_context[arg_name] = arg_value
        return context.eval_context[arg_name]

    # return argument converted to upper case
    def upper_case(self, context, arg):
        return arg.upper()

    # return argument converted to lower case
    def lower_case(self, context, arg):
        return arg.lower()


# an example class for a XSLT extension
#   - currently only emits some text
class XSLTExtensionAssign(etree.XSLTExtension):
    def execute(self, context, self_node, input_node, output_parent):
        output_parent.text = "Hello from Python!"


def main():
    # parse command line arguments
    parser = argparse.ArgumentParser(description="xsltproc for Python")
    parser.add_argument('stylesheet', help="path to XSLT file")
    parser.add_argument('file', help="path to XML file")
    parser.add_argument('--output', '-o', help="output file")
    parser.add_argument('--param', '-p', help="pass an XPath parameter as name/value pair",
                        nargs=2, action='append', metavar=('NAME', 'VALUE'), default=[])
    parser.add_argument('--stringparam', '-s', help="pass a string parameter as name/value pair",
                        nargs=2, action='append', metavar=('NAME', 'VALUE'), default=[])
    args = parser.parse_args()

    # check if input files exist
    for file in [args.stylesheet, args.file]:
        if not os.path.isfile(file):
            print("Error: '%s' does not exist" % file, file=sys.stderr)
            exit(1)

    # set up extensions and bind to "inkscape" namespace
    ext_module1 = XPathExtensions()
    ext_module2 = XSLTExtensionAssign()

    NS = 'org.inkscape.xslt.files'  # old java class path name but could basically be anything

    # extensions1 = etree.Extension(ext_module, function_mapping=None, ns=NS)
    # extensions2 = { (NS, 'xslt-example') : ext_module2 }
    # extensions = {}
    # extensions.update(extensions1)
    # extensions.update(extensions2)

    extensions = {(NS, 'exists')       : ext_module1.exists,
                  (NS, 'run')          : ext_module1.run,
                  (NS, 'run-shell')    : ext_module1.run_shell,
                  (NS, 'get-variable') : ext_module1.get_variable,
                  (NS, 'set-variable') : ext_module1.set_variable,
                  (NS, 'upper-case')   : ext_module1.upper_case,
                  (NS, 'lower-case')   : ext_module1.lower_case,
                  (NS, 'xslt-example') : ext_module2}

    # prepare dict with all parameters
    stringparams = dict(args.stringparam)
    for key in stringparams:
        stringparams[key] = etree.XSLT.strparam(stringparams[key])
    params = dict(args.param)
    params.update(stringparams)

    # do the parsing, then transform the XSLT
    xslt = etree.parse(args.stylesheet)
    dom = etree.parse(args.file)
    transform = etree.XSLT(xslt, extensions=extensions)
    try:
        newdom = transform(dom, **params)
        errorlevel = 0
    except etree.XSLTError:
        errorlevel = 1

    # print all informational output from the transformations run to stderr
    # unfortunately there is no way to distinguish between user messages, warnings and error messages
    for entry in transform.error_log:
        print("->line %s, col %s: %s" % (entry.line, entry.column, entry.message), file=sys.stderr)
        print("  domain: %s (%d)" % (entry.domain_name, entry.domain), file=sys.stderr)
        print("  type: %s (%d)" % (entry.type_name, entry.type), file=sys.stderr)
        print("  level: %s (%d)" % (entry.level_name, entry.level), file=sys.stderr)
        print("  filename: %s" % entry.filename, file=sys.stderr)

    # create output (or fail on error)
    if errorlevel:
        print(">>> Fatal error while processing files, aborting...", file=sys.stderr)
        exit(1)
    else:
        if args.output:
            f = open(args.output, 'wb')
        else:
            # open the binary buffer of stdout as the output is already encoded
            try:
                f = sys.stdout.buffer
            except AttributeError:
                f = sys.stdout
        newdom.write_output(f)


if __name__ == '__main__':
    main()
